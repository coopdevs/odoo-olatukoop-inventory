---
# Use python3 interpreter
ansible_python_interpreter: "/usr/bin/python3"

# Odoo vars
odoo_role_odoo_core_modules_dict:
  shared:
    - account
    - account_invoicing
    - account_asset
    - account_analytic_default
    - account_bank_statement_import
    - account_cancel
    - analytic
    - auth_crypt
    - auth_signup
    - base
    - base_automation
    - base_iban
    - base_import
    - base_setup
    - base_vat
    - base_vat_autocomplete
    - contacts
    - crm
    - decimal_precision
    - document
    - hr
    - hr_attendance
    - hr_contract
    - hr_employee
    - hr_expense
    - hr_holidays
    - hr_timesheet
    - l10n_es
    - product
    - purchase
    - sale_management
    - web
    - web_diagram
    - web_kanban_gauge
    - web_planner
    - web_settings_dashboard
  talaios:
    - website
  dev-talaios:
    - website

odoo_role_odoo_community_modules_dict:
  shared:
    - account_asset_management
    - account_banking_mandate
    - account_banking_pain_base
    - account_banking_sepa_credit_transfer
    - account_banking_sepa_direct_debit
    - account_due_list
    - account_financial_report
    - account_payment_auto_partner_bank
    - account_payment_mode
    - account_payment_order
    - account_payment_partner
    - base_bank_from_iban
    - base_technical_features
    - contract
    - contract_variable_qty_timesheet
    - contract_variable_quantity
    - dbfilter_from_header
    - l10n_es_account_asset
    - l10n_es_account_bank_statement_import_n43
    - l10n_es_account_invoice_sequence
    - l10n_es_aeat
    - l10n_es_aeat_mod111
    - l10n_es_aeat_mod115
    - l10n_es_aeat_mod303
    - l10n_es_aeat_mod347
    - l10n_es_aeat_mod349
    - l10n_es_mis_report
    - l10n_es_partner
    - l10n_es_toponyms
    - mass_editing
    - mis_builder
    - mis_builder_budget
    - project_status
    - project_task_default_stage
    - project_task_dependency
    - project_template
    - project_timeline
    - project_timeline_task_dependency
    - web_decimal_numpad_dot
    - web_favicon
    - web_no_bubble
    - web_responsive
    - web_searchbar_full_width
  odoo-local:
    - l10n_es_ticketbai
    - l10n_es_ticketbai_api
  talaios:
    - agreement
    - agreement_legal
    - helpdesk_mgmt
    - helpdesk_mgmt_project
    - helpdesk_motive
    - helpdesk_type
    - l10n_es_ticketbai
    - l10n_es_ticketbai_api
  dev-talaios:
    - agreement
    - agreement_legal
    - helpdesk_mgmt
    - helpdesk_mgmt_project
    - helpdesk_motive
    - helpdesk_type
    - l10n_es_ticketbai
    - l10n_es_ticketbai_api
  skuramobile:
    - l10n_es_ticketbai
    - l10n_es_ticketbai_api
  odoo-test-skuramobile:
    - l10n_es_ticketbai
    - l10n_es_ticketbai_api
  tapuntu:
    - l10n_es_ticketbai
    - l10n_es_ticketbai_api
  odoo-test-tapuntu:
    - l10n_es_ticketbai
    - l10n_es_ticketbai_api


odoo_role_odoo_user: odoo
odoo_role_download_strategy: tar
odoo_role_odoo_version: "12.0"
odoo_role_odoo_release: "12.0_2022-02-01"
odoo_role_odoo_url: "https://gitlab.com/coopdevs/OCB/-/archive/{{ odoo_role_odoo_release }}/OCB-{{ odoo_role_odoo_release }}.tar.gz"

# We want clean instances without example data in apps
odoo_role_demo_data: false

# Odoo provisioning
odoo_provisioning_version: "v0.7.6"
# Enable list_db while we don't change the backup strategy at odoo-provisioning
odoo_role_list_db: true

# Production security defaults
odoo_role_odoo_http_interface: '127.0.0.1'
odoo_role_odoo_proxy_mode: true

# Nginx configuration
nginx_configs:
  upstream:
    - |
      map $http_host $odoo_dbfilter {
        'odoo.talaios.coop' talaios;
        'odoo.skuramobile.eus' skuramobile;
        'odoo.tapuntu.eus' tapuntu;
        'dev-odoo.talaios.coop' dev-talaios;
        'odootest.tapuntu.eus' odoo-test-tapuntu;
        'odoo.test.skuramobile.com' odoo-test-skuramobile;
      }
      upstream odoo { server 127.0.0.1:8069; }
    - upstream nexporter { server 127.0.0.1:9100; }
    - upstream pexporter { server 127.0.0.1:9187; }

nginx_sites:
  odoo:
    - |
      listen 80;
      server_name {{ domains | default([inventory_hostname]) | join(' ') }};
      rewrite ^(.*)$ https://$host$1 permanent;
  odoo.ssl:
    - |
      listen 443 ssl;
      ssl_certificate /etc/letsencrypt/live/{{ inventory_hostname }}/fullchain.pem;
      ssl_certificate_key /etc/letsencrypt/live/{{ inventory_hostname }}/privkey.pem;
      include /etc/letsencrypt/options-ssl-nginx.conf;
      ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;
      server_name {{ domains | default([inventory_hostname]) | join(' ') }};
      proxy_read_timeout 720s;
      proxy_connect_timeout 720s;
      proxy_send_timeout 720s;
      proxy_set_header X-Forwarded-Host $host;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header X-Forwarded-Proto $scheme;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Odoo-dbfilter $odoo_dbfilter;
      ssl on;
      access_log /var/log/nginx/odoo.access.log;
      error_log /var/log/nginx/odoo.error.log;
      location / {
        proxy_redirect off;
        proxy_pass http://odoo;
      }
      location /node/ {
        include proxy_params;
        proxy_redirect off;
        proxy_pass http://nexporter/metrics;
        auth_basic           "closed site";
        auth_basic_user_file /etc/odoo/.htpasswd;
      }
      location /postgres/ {
        include proxy_params;
        proxy_redirect off;
        proxy_pass http://pexporter/metrics;
        auth_basic           "closed site";
        auth_basic_user_file /etc/nginx/.pexporter.htpasswd;
      }
      gzip_types text/css text/less text/plain text/xml application/xml application/json application/javascript;
      gzip on;
